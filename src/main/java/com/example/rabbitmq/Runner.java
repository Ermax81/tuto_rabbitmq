package com.example.rabbitmq;

import com.example.rabbitmq.config.AMQPProducer;
import com.example.rabbitmq.config.RabbitMQProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    RabbitMQProperties rabbitMQProperties;

    @Autowired
    private AMQPProducer producer;

    public Runner(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        Notification notif = new Notification();
        notif.setNotificationType("Email");
        notif.setMsg("A new mail from local test");

        producer.sendMessage(notif);
    }
}
