package com.example.rabbitmq.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
@Setter
@Getter
public class RabbitMQProperties {

    private String queueName;
    private String exchangeName;
    private String routingKey;

}