package com.example.rabbitmq.config;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;

@Configuration
public class AMQPConfig {

    @Autowired
    private RabbitMQProperties rabbitMQProperties;

    //There are two types of Queue - durable and non-durable. Durable queue survives a server restart.
    @Bean
    Queue queue() {
        return new Queue(rabbitMQProperties.getQueueName(), false);
    }

    //Topic Exchange routes messages to multiple queues by a partial matching of a routing key. It uses patterns to match the routing and binding key whereas direct exchange routes messages to a queue by matching routing key equal to binding key.
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(rabbitMQProperties.getExchangeName());
    }

    //We are using TopicExchange here but Direct exchange can also be used and it depends on the requirement.
    /*@Bean
    DirectExchange exchange() {
        return new DirectExchange(rabbitMQProperties().getExchangeName());
    }*/

    //The binding() method binds these two together, defining the behavior that occurs when RabbitTemplate publishes to an exchange.
    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(rabbitMQProperties.getRoutingKey());
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(rabbitMQProperties.getQueueName());
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    @Bean
    public RabbitTemplate amqpTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }

    //By default Spring Boot uses org.springframework.amqp.support.converter.SimpleMessageConverter and serialize the object into byte[].
    // Hence, we have Jackson2JsonMessageConverter to send the message in a JSON format.
    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    //The bean defined in the listenerAdapter() method is registered as a message listener in the container defined in container().
    // It will listen for messages on the queue.
    // Because the RabbitMqListener class is a POJO, it needs to be wrapped in the MessageListenerAdapter, where you specify it to invoke listen().
    @Bean
    MessageListenerAdapter listenerAdapter(RabbitMQListener listener) {
        return new MessageListenerAdapter(listener, "listen");
    }

}