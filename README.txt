version 1.0.0
Source: https://spring.io/guides/gs/messaging-rabbitmq/
Repo original: git clone https://github.com/spring-guides/gs-messaging-rabbitmq.git

version 1.1.0
Source: https://www.devglan.com/spring-boot/springboot-rabbitmq-example


Etape 1:
$ docker-compose up -d

Etape 2: Build - creation du jar:
$ mvn clean package

Etape 3: Test:
$ java -jar target/rabbitmq-0.0.1-SNAPSHOT.jar

Etape final :
$ docker-compose down

Console output:
Send msg = Notification(notificationType=Email, msg=A new mail from local test)
Received a new notification...
Notification(notificationType=Email, msg=A new mail from local test)
